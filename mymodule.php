<?php
if (!defined('_PS_VERSION_')) {
    exit;
}
class MyModule extends Module
{
    public function __construct()
    {
        $this->name = 'mymodule';
        $this->tab = 'other';
        $this->version = '1.0.0';
        $this->author = 'Qiong Tang';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('My module');
        $this->description = $this->l('Description of my module.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('MYMODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
    
        return parent::install() &&
            $this->registerHook('leftColumn') &&
            $this->registerHook('actionFrontControllerSetMedia') &&
            $this->registerHook('displayHeader') &&
            Configuration::updateValue('MYMODULE_NAME', 'my friend');
    }

    public function uninstall()
    {
    if (!parent::uninstall() ||
        !Configuration::deleteByName('MYMODULE_NAME')
    ) {
        return false;
    }

    return true;
    }
    //show some text in the header
   public function hookDisplayHeader(){
    $this->context->smarty->assign([
        'my_module_name' => Configuration::get('MYMODULE_NAME')]);
       return $this->display(__FILE__,'views/templates/hook/home.tpl');
   }

   public function hookDisplayLeftColumn($params)
   {
       //store some variables in smarty
       $this->context->smarty->assign([
           'my_module_name' => Configuration::get('MYMODULE_NAME'),
           'my_module_link' => $this->context->link->getModuleLink('mymodule', 'display'),
           /*get  the link:http://localhost/prestashop/en/module/mymodule/display*/
           'my_module_message' => $this->l('This is a simple text message') 
       ]);

       return $this->display(__FILE__, 'mymodule.tpl');
   }

   public function hookDisplayRightColumn($params)
   {
       return $this->hookDisplayLeftColumn($params);
   }
//not working the css and js
   public function hookActionFrontControllerSetMedia()
   {
       $this->context->controller->registerStylesheet(
           'mymodule-style',
           $this->_path.'views/css/mymodule.css',
           [
               'media' => 'all',
               'priority' => 1000,
           ]
       );

       $this->context->controller->registerJavascript(
           'mymodule-javascript',
           $this->_path.'views/js/mymodule.js',
           [
               'position' => 'bottom',
               'priority' => 1000,
           ]
       );
   }

    //add the configuration page
    public function getContent()
    {
        $output = null;
        //check if the form has been submited
        if (Tools::isSubmit('submit'.$this->name)) {
            //get the value by _post or _ get method
            $myModuleName = strval(Tools::getValue('MYMODULE_NAME'));

            if (
                //check if the input value is validate
                !$myModuleName ||
                empty($myModuleName) ||
                !Validate::isGenericName($myModuleName)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('MYMODULE_NAME', $myModuleName);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        return $output.$this->displayForm();
    }

    //display the config form
    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->l('Module name'),
                    'name' => 'MYMODULE_NAME',
                    'size' => 20,
                    'required' => true
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            ]
        ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];

        // Load current value
        $helper->fields_value['MYMODULE_NAME'] = Tools::getValue('MYMODULE_NAME', Configuration::get('MYMODULE_NAME'));

        return $helper->generateForm($fieldsForm);
    }
}
