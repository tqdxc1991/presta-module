<?php
class mymoduledisplayModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        /**assign some values to some variables */
        $this->context->smarty->assign(array(
                            /**connect to the database and make some queries */
            'nb_product' => Db::getInstance()->getValue('SELECT COUNT(*) FROM `'._DB_PREFIX_.'product`'),
            'categories' => Db::getInstance()->executeS('SELECT `name` FROM `'._DB_PREFIX_.'category_lang` ORDER BY `id_lang` ASC')

        ));
        $this->setTemplate('module:mymodule/views/templates/front/display.tpl');
        /**set the template for the module link :http://localhost/prestashop/en/module/mymodule/display */
    }
}

