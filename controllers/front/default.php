<?php
class mymoduledefaultModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $this->setTemplate('module:mymodule/views/templates/front/default.tpl');
    }
}
